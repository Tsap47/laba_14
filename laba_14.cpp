﻿// laba_14.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <cmath>
#include <conio.h>
#include <chrono>
using namespace std;

class Timer
{
private:
	// Псевдонимы типов используются для удобного доступа к вложенным типам
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};
int main()
{
	Timer t;
	setlocale(LC_ALL, "Russian");
	long long N;
	long long i;
	long long res;
	cout << "Введите число N от 1 до 10.000" << endl;
	cin >> N;
	res = 1;
	for (i = 1; i <= N; i++) {
		res = res * i;
	}
	cout <<"Факториал "<< N << ": " << res << endl;
	cout << "Робот вычислил факториал за: " << t.elapsed();
}

